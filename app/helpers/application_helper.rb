# frozen_string_literal: true

module ApplicationHelper
  def full_title(page_title = '')
    if page_title.blank?
      "BIGBAG Store"
    else
      "#{page_title} - BIGBAG Store"
    end
  end
end
