require 'rails_helper'
RSpec.describe Potepan::CategoriesController, type: :controller do
  let!(:taxonomy) { create(:taxonomy, name: 'TestTaxonomy') }
  let!(:taxon) { create(:taxon, name: 'TestTaxon', taxonomy: taxonomy) }
  let!(:product) { create(:product, name: 'TestProduct', taxons: [taxon]) }

  describe "categories#showのテスト" do

    before do
      get :show, params: { id: taxon.id }
    end

    it "showがレンダリングされていること" do
      expect(response).to render_template :show
    end

    it 'インスタンス変数が設定されていること' do
      expect(assigns(:taxon)).to eq taxon
      expect(assigns(:taxonomies).first).to eq taxonomy
      expect(assigns(:products)).to eq taxon.products
    end
  end
end
