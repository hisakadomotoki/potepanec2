require 'rails_helper'
RSpec.describe Potepan::ProductsController, type: :controller do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let!(:product) { create(:product, name: 'Test Coat', taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }


  describe "product#showのテスト" do

    before do
      get :show, params: { id: product.id }
    end

    it "showがレンダリングされていること" do
      expect(response).to render_template :show
    end

    it 'インスタンス変数が設定されていること' do
      expect(assigns(:product)).to eq product
    end

    it "4つの関連商品が存在する" do
      expect(assigns(:related_products).count).to eq 4
    end
  end
end