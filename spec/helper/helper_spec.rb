require 'rails_helper'

RSpec.describe "Helper", type: :helper do

  describe 'タイトル表示のテスト' do
    include ApplicationHelper

    context '引数がある場合' do
      let(:page_title) { 'Test Coat' }
      it '<引数> - BIGBAG Store と表示すること' do
        expect(full_title(page_title)).to eq("Test Coat - BIGBAG Store")
      end
    end

    context '引数がない場合' do
      let(:page_title) { '' }
      it 'BIGBAG Store と表示すること' do
        expect(full_title).to eq("BIGBAG Store")
      end
    end

    context '引数がnilの場合' do
      let(:page_title) { nil }
      it 'BIGBAG Store と表示すること' do
        expect(full_title).to eq("BIGBAG Store")
      end
    end
  end
end