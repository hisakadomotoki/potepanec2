require 'rails_helper'

RSpec.feature "Categories", type: :feature do

  let(:taxonomy) { create(:taxonomy) }
  let(:taxon)    { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:diff_product) { create(:product) }

  describe 'カテゴリー一覧ページのテスト' do

    before do
      visit potepan_category_path(taxon.id)
    end

    it 'サイドバーにカテゴリー情報を表示すること' do
      within '.side-nav' do
        expect(page).to have_content taxonomy.name
        expect(page).to have_content taxon.name
      end
    end

    it '対象の商品情報を表示すること' do
      within ".productsContent" do
        expect(page).to have_content product.name
      end
    end

    it '対象でない商品情報を表示しないこと' do
      within ".productsContent" do
        expect(page).not_to have_content diff_product.name
      end
    end

    it 'Homeリンクが正常動作すること' do
      click_link "HOME", match: :prefer_exact
      expect(current_path).to eq potepan_index_path
    end

    it '商品ページリンクが正常動作すること' do
      within ".productsContent" do
        click_on taxon.name
        expect(page).to have_content product.name
        click_on product.name
        expect(current_path).to eq potepan_product_path(product.id)
      end
    end
  end
end