require 'rails_helper'

RSpec.feature "Products", type: :feature do

  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let(:product) { create(:product, taxons: [taxon]) }
  let!(:no_related_product) { create(:product, name: 'no_related_product') }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

  describe '商品詳細ページのテスト' do

    before do
      visit potepan_product_path(product.id)
    end

    it '主商品を画面中央に表示すること' do
      within ".singleProduct" do
        expect(page).to have_content product.name
        expect(page).to have_content product.display_price
        expect(page).to have_content product.description
      end
    end

    it '関連商品の情報を画面に4件表示すること' do
      within ".productsContent" do
        expect(page).to have_content related_products[0].name
        expect(page).to have_content related_products[1].name
        expect(page).to have_content related_products[2].name
        expect(page).to have_content related_products[3].name
      end
    end

    it '主商品を関連商品欄に表示しないこと' do
      within ".productsContent" do
        expect(page).not_to have_content no_related_product.name
      end
    end

    it '関連しない商品を表示しないこと' do
      within ".productsContent" do
        expect(page).not_to have_content no_related_product.name
      end
    end
    
    it '5件目の関連商品の情報を表示しないこと' do
      within ".productsContent" do
        expect(page).to have_content('Product', count:4)
      end
    end

    it '関連商品欄のリンクが正常動作すること' do
      within ".productsContent" do
        expect(page).to have_content related_products[0].name
        click_on related_products[0].name
        expect(current_path).to eq potepan_product_path(related_products[0].id)
      end
    end

    it 'Homeリンクが正常動作すること' do
      click_link "HOME", match: :prefer_exact
      expect(current_path).to eq potepan_index_path
    end

    it 'カテゴリーページリンクが正常動作すること' do
      expect(page).to have_link '一覧ページへ戻る'
      click_on '一覧ページへ戻る', match: :prefer_exact
      expect(current_path).to eq potepan_category_path(taxon.id)
    end
  end
end